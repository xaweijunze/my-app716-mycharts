import {Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import * as echarts from "echarts";

@Directive({
    selector: 'echart'
})
export class EChartOptionDirective1 implements OnInit,OnChanges{

    @Input('chartOption')
    chartOption: Object;
    constructor(private el: ElementRef) {}

    public ngOnInit(): void {
    }

    public ngOnChanges(changes: SimpleChanges): void {
        let echartDom = echarts.init(this.el.nativeElement);
        echartDom.clear();
        echartDom.setOption(this.chartOption);
    }

}
