import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgxEchartsModule } from 'ngx-echarts';
import { VisibilityContainerComponent } from './core/visibility-container/visibility-container.component';

import { FlexibleService } from './flexible.service';
import { VCNoComponent } from './core/v-c-no/v-c-no.component';
import { VCMapComponent } from './core/v-c-map/v-c-map.component'
import { EChartOptionDirective1 } from './echart-option.directive'
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    VisibilityContainerComponent,
    VCNoComponent,
    VCMapComponent,
    EChartOptionDirective1
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    HttpClientModule

  ],
  providers: [
    FlexibleService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
