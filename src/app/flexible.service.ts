import {Injectable} from '@angular/core';

@Injectable()
export class FlexibleService {
  constructor() {
  }
  flexible(docEl) {

    //设定本盒子内em的值
    function setRemUnit() {
      if( docEl ){
        var em = docEl.clientWidth / 24;
        docEl.style.fontSize = em + "px";
      }
    }
    setRemUnit();
    // reset rem unit on page resize
    window.addEventListener("resize", setRemUnit);
    window.addEventListener("pageshow", function (e) {
      if (e.persisted) {
        setRemUnit();
      }
    });
  }

}
