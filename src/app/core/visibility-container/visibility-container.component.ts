import {Component, OnInit} from '@angular/core';
import {FlexibleService} from '../../flexible.service'
import {EChartOption} from 'echarts';
import * as echarts from 'echarts';
import {interval, Observable} from 'rxjs';

// import set = Reflect.set;

@Component({
  selector: 'app-visibulity-container',
  templateUrl: './visibility-container.component.html',
  styleUrls: ['./visibility-container.component.less']
})
export class VisibilityContainerComponent implements OnInit {

  bar1option: EChartOption;
  bar2option: EChartOption;
  pie1option: EChartOption;
  pie2option: EChartOption;
  line1option: EChartOption;
  line2option: EChartOption;
  time;

  constructor(private flexible: FlexibleService) {
  }

  ngOnInit(): void {
    this.getNowDate();
    this.flexible.flexible(document.getElementById('container_body')); //容器font-size的适配
    this.bar1(echarts.init(document.getElementById('bar1chart')));
    this.bar2(echarts.init(document.getElementById('bar2chart')));
    this.line1(echarts.init(document.getElementById('line1chart')));
    this.line2(echarts.init(document.getElementById('line2chart')));
    this.pie1(echarts.init(document.getElementById('line1chart')));
    this.pie2(echarts.init(document.getElementById('pie2chart')));

  }

  getNowDate() {
    let that = this;
    let source = interval(1000);
    source.subscribe({
      next: function (value) {
        let d = new Date();
        let year = d.getFullYear();
        let month = d.getMonth() + 1;
        let date = d.getDate();
        let hour = (d.getHours() < 10) ? ("0" + d.getHours()) : d.getHours();
        let min = (d.getMinutes() < 10) ? ("0" + d.getMinutes()) : d.getMinutes();
        let sec = (d.getSeconds() < 10) ? ("0" + d.getSeconds()) : d.getSeconds();
        let now = year + "年" + month + "月" + date + "日-" + hour + "点" + min + "分" + sec + "秒";
        that.time = now;
      },
      complete: function () {
        console.log('complete!');
      },
      error: function (error) {
        console.log('Throw Error: ' + error)
      }
    });

    // console.log(this.time)


  }

  bar1(bar1Ele) {
    // 2. 指定配置项和数据
    this.bar1option = {
      color: ["#2f89cf"],
      tooltip: {
        trigger: "axis",
        axisPointer: {
          // 坐标轴指示器，坐标轴触发有效
          type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      // 修改图表的大小
      grid: {
        left: "0%",
        top: "10px",
        right: "2%",
        bottom: "4%",
        containLabel: true
      },
      xAxis: [
        {
          type: "category",
          data: [
            "旅游行业",
            "教育培训",
            "游戏行业",
            "医疗行业",
            "电商行业",
            "社交行业",
            "金融行业"
          ],
          axisTick: {
            alignWithLabel: true
          },
          // 修改刻度标签 相关样式
          axisLabel: {
            color: "rgba(255,255,255,.6) ",
            fontSize: "12"
          },
          // 不显示x坐标轴的样式
          axisLine: {
            show: false
          }
        }
      ],
      yAxis: [
        {
          type: "value",
          // 修改刻度标签 相关样式
          axisLabel: {
            color: "rgba(255,255,255,.6) ",
            fontSize: 12
          },
          // y轴的线条改为了 2像素
          axisLine: {
            lineStyle: {
              color: "rgba(255,255,255,.1)",
              width: 2
            }
          },
          // y轴分割线的颜色
          splitLine: {
            lineStyle: {
              color: "rgba(255,255,255,.1)"
            }
          }
        }
      ],
      series: [
        {
          name: "直接访问",
          type: "bar",
          barWidth: "35%",
          data: [200, 300, 300, 900, 1500, 1200, 600],
          itemStyle: {
            // 修改柱子圆角
            barBorderRadius: 5
          }
        }
      ]
    };
    // 3. 把配置项给实例对象
    // 4. 让图表跟随屏幕自动的去适应
    window.addEventListener("resize", function () {
      bar1Ele.resize();
    });
  }

  bar2(bar2Ele) {
    // 2. 指定配置项和数据
    this.bar2option = {
      grid: {
        top: "10%",
        left: "22%",
        bottom: "10%"
        // containLabel: true
      },
      // 不显示x轴的相关信息
      xAxis: {
        show: false
      },
      yAxis: [
        {
          type: "category",
          inverse: true,
          data: ["HTML5", "CSS3", "javascript", "VUE", "NODE"],
          // 不显示y轴的线
          axisLine: {
            show: false
          },
          // 不显示刻度
          axisTick: {
            show: false
          },
          // 把刻度标签里面的文字颜色设置为白色
          axisLabel: {
            color: "#fff"
          }
        },
        {
          data: [702, 350, 610, 793, 664],
          inverse: true,
          // 不显示y轴的线
          axisLine: {
            show: false
          },
          // 不显示刻度
          axisTick: {
            show: false
          },
          // 把刻度标签里面的文字颜色设置为白色
          axisLabel: {
            color: "#fff"
          }
        }
      ],
      series: [
        {
          name: "条",
          type: "bar",
          data: [
            {
              value: 70,
              itemStyle: {
                color: "#1089E7"
              }
            },
            {
              value: 34,
              itemStyle: {
                color: "#F57474"
              }
            },
            {
              value: 60,
              itemStyle: {
                color: "#56D0E3"
              }
            },
            {
              value: 78,
              itemStyle: {
                color: "#F8B448"
              }
            },
            {
              value: 69,
              itemStyle: {
                color: "#8B78F6"
              }
            },
          ],
          yAxisIndex: 0,
          // 修改第一组柱子的圆角
          itemStyle: {
            barBorderRadius: 20,
            // 此时的color 可以修改柱子的颜色
            // color: function(params) {
            //   // params 传进来的是柱子对象
            //   // console.log(params);
            //   // dataIndex 是当前柱子的索引号
            //   return [params.dataIndex];
            // }
          },
          // 柱子之间的距离
          barCategoryGap: 50,
          //柱子的宽度
          barWidth: 10,
          // 显示柱子内的文字
          label: {
            show: true,
            position: "inside",
            // {c} 会自动的解析为 数据  data里面的数据
            formatter: "{c}%"
          }
        },
        {
          name: "框",
          type: "bar",
          barCategoryGap: 50,
          barWidth: 15,
          yAxisIndex: 1,
          data: [100, 100, 100, 100, 100],
          itemStyle: {
            color: "none",
            borderColor: "#00c1de",
            borderWidth: 3,
            barBorderRadius: 15
          }
        }
      ]
    };
    // 3. 把配置项给实例对象
    // 4. 让图表跟随屏幕自动的去适应
    window.addEventListener("resize", function () {
      bar2Ele.resize();
    });
  }

  line1(line1Ele) {
    var data = {
      year: [
        [24, 40, 101, 134, 90, 230, 210, 230, 120, 230, 210, 120],
        [40, 64, 191, 324, 290, 330, 310, 213, 180, 200, 180, 79]
      ]
    };
    // 2. 指定配置项和数据
    this.line1option = {
      color: ["#00f2f1", "#ed3f35"],
      tooltip: {
        // 通过坐标轴来触发
        trigger: "axis"
      },
      legend: {
        // 距离容器10%
        right: "10%",
        // 修饰图例文字的颜色
        textStyle: {
          color: "#4c9bfd"
        }
        // 如果series 里面设置了name，此时图例组件的data可以省略
        // data: ["邮件营销", "联盟广告"]
      },
      grid: {
        top: "20%",
        left: "3%",
        right: "4%",
        bottom: "3%",
        show: true,
        borderColor: "#012f4a",
        containLabel: true
      },

      xAxis: {
        type: "category",
        boundaryGap: false,
        data: [
          "1月",
          "2月",
          "3月",
          "4月",
          "5月",
          "6月",
          "7月",
          "8月",
          "9月",
          "10月",
          "11月",
          "12月"
        ],
        // 去除刻度
        axisTick: {
          show: false
        },
        // 修饰刻度标签的颜色
        axisLabel: {
          color: "rgba(255,255,255,.7)"
        },
        // 去除x坐标轴的颜色
        axisLine: {
          show: false
        }
      },
      yAxis: {
        type: "value",
        // 去除刻度
        axisTick: {
          show: false
        },
        // 修饰刻度标签的颜色
        axisLabel: {
          color: "rgba(255,255,255,.7)"
        },
        // 修改y轴分割线的颜色
        splitLine: {
          lineStyle: {
            color: "#012f4a"
          }
        }
      },
      series: [
        {
          name: "新增粉丝",
          type: "line",
          stack: "总量",
          // 是否让线条圆滑显示
          smooth: true,
          data: data.year[0]
        },
        {
          name: "新增游客",
          type: "line",
          stack: "总量",
          smooth: true,
          data: data.year[1]
        }
      ]
    };
    // 3. 把配置项给实例对象
    // 4. 让图表跟随屏幕自动的去适应
    window.addEventListener("resize", function () {
      line1Ele.resize();
    });
  }

  line2(line2Ele) {
    //指定配置项和数据
    this.line2option = {
      tooltip: {
        trigger: "axis",
        axisPointer: {
          lineStyle: {
            color: "#dddc6b"
          }
        }
      },
      legend: {
        top: "0%",
        textStyle: {
          color: "rgba(255,255,255,.5)",
          fontSize: "12"
        }
      },
      grid: {
        left: "10",
        top: "30",
        right: "10",
        bottom: "10",
        containLabel: true
      },

      xAxis: [
        {
          type: "category",
          boundaryGap: false,
          axisLabel: {
            textStyle: {
              color: "rgba(255,255,255,.6)",
              fontSize: 12
            }
          },
          axisLine: {
            lineStyle: {
              color: "rgba(255,255,255,.2)"
            }
          },

          data: [
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30"
          ]
        },
        {
          axisPointer: {show: false},
          axisLine: {show: false},
          position: "bottom",
          offset: 20
        }
      ],

      yAxis: [
        {
          type: "value",
          axisTick: {show: false},
          axisLine: {
            lineStyle: {
              color: "rgba(255,255,255,.1)"
            }
          },
          axisLabel: {
            textStyle: {
              color: "rgba(255,255,255,.6)",
              fontSize: 12
            }
          },

          splitLine: {
            lineStyle: {
              color: "rgba(255,255,255,.1)"
            }
          }
        }
      ],
      series: [
        {
          name: "播放量",
          type: "line",
          smooth: true,
          symbol: "circle",
          symbolSize: 5,
          showSymbol: false,
          lineStyle: {
            normal: {
              color: "#0184d5",
              width: 2
            }
          },
          areaStyle: {
            normal: {

              shadowColor: "rgba(0, 0, 0, 0.1)"
            }
          },
          itemStyle: {
            normal: {
              color: "#0184d5",
              borderColor: "rgba(221, 220, 107, .1)",
              borderWidth: 12
            }
          },
          data: [
            30,
            40,
            30,
            40,
            30,
            40,
            30,
            60,
            20,
            40,
            20,
            40,
            30,
            40,
            30,
            40,
            30,
            40,
            30,
            60,
            20,
            40,
            20,
            40,
            30,
            60,
            20,
            40,
            20,
            40
          ]
        },
        {
          name: "转发量",
          type: "line",
          smooth: true,
          symbol: "circle",
          symbolSize: 5,
          showSymbol: false,
          lineStyle: {
            normal: {
              color: "#00d887",
              width: 2
            }
          },
          areaStyle: {
            normal: {

              shadowColor: "rgba(0, 0, 0, 0.1)"
            }
          },
          itemStyle: {
            normal: {
              color: "#00d887",
              borderColor: "rgba(221, 220, 107, .1)",
              borderWidth: 12
            }
          },
          data: [
            50,
            30,
            50,
            60,
            10,
            50,
            30,
            50,
            60,
            40,
            60,
            40,
            80,
            30,
            50,
            60,
            10,
            50,
            30,
            70,
            20,
            50,
            10,
            40,
            50,
            30,
            70,
            20,
            50,
            10,
            40
          ]
        }
      ]
    };
    // 3. 把配置项给实例对象
    // 4. 让图表跟随屏幕自动的去适应
    window.addEventListener("resize", function () {
      line2Ele.resize();
    });
  }

  pie1(pie1Ele) {
    //指定配置项和数据
    this.pie1option = {
      tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b}: {c} ({d}%)",
        position: function (p) {
          //其中p为当前鼠标的位置
          return [p[0] + 10, p[1] - 10];
        }
      },
      legend: {
        top: "90%",
        itemWidth: 10,
        itemHeight: 10,
        data: ["0岁以下", "20-29岁", "30-39岁", "40-49岁", "50岁以上"],
        textStyle: {
          color: "rgba(255,255,255,.5)",
          fontSize: "12"
        }
      },
      series: [
        {
          name: "年龄分布",
          type: "pie",
          center: ["50%", "42%"],
          radius: ["40%", "60%"],
          roseType: "radius",
          color: [
            "#065aab",
            "#066eab",
            "#0682ab",
            "#0696ab",
            "#06a0ab",
            "#06b4ab",
            "#06c8ab",
            "#06dcab",
            "#06f0ab"
          ],
          label: {show: false},
          labelLine: {show: false},
          data: [
            {value: 1, name: "0岁以下"},
            {value: 4, name: "20-29岁"},
            {value: 2, name: "30-39岁"},
            {value: 2, name: "40-49岁"},
            {value: 1, name: "50岁以上"}
          ]
        }
      ]
    };
    // 3. 把配置项给实例对象
    // 4. 让图表跟随屏幕自动的去适应
    window.addEventListener("resize", function () {
      pie1Ele.resize();
    });
  }

  pie2(pie2Ele) {
    //指定配置项和数据
    this.pie2option = {
      legend: {
        top: "90%",
        itemWidth: 10,
        itemHeight: 10,
        textStyle: {
          color: "rgba(255,255,255,.5)",
          fontSize: "12"
        }
      },
      tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      // 注意颜色写的位置
      color: [
        "#006cff",
        "#60cda0",
        "#ed8884",
        "#ff9f7f",
        "#0096ff",
        "#9fe6b8",
        "#32c5e9",
        "#1d9dff"
      ],
      series: [
        {
          name: "点位统计",
          type: "pie",
          // 如果radius是百分比则必须加引号
          radius: ["10%", "70%"],
          center: ["50%", "42%"],
          roseType: "radius",
          data: [
            {value: 20, name: "云南"},
            {value: 26, name: "北京"},
            {value: 24, name: "山东"},
            {value: 25, name: "河北"},
            {value: 20, name: "江苏"},
            {value: 25, name: "浙江"},
            {value: 30, name: "深圳"},
            {value: 42, name: "广东"}
          ],
          // 修饰饼形图文字相关的样式 label对象
          label: {
            fontSize: 10
          },
          // 修饰引导线样式
          labelLine: {
            // 连接到图形的线长度
            length: 10,
            // 连接到文字的线长度
            length2: 10
          }
        }
      ]
    };
    // 3. 把配置项给实例对象
    // 4. 让图表跟随屏幕自动的去适应
    window.addEventListener("resize", function () {
      pie2Ele.resize();
    });
  }
}
