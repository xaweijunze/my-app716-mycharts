import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisibilityContainerComponent } from './visibility-container.component';

describe('VisibilityContainerComponent', () => {
  let component: VisibilityContainerComponent;
  let fixture: ComponentFixture<VisibilityContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisibilityContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisibilityContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
