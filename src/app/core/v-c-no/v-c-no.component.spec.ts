import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VCNoComponent } from './v-c-no.component';

describe('VCNoComponent', () => {
  let component: VCNoComponent;
  let fixture: ComponentFixture<VCNoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VCNoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VCNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
