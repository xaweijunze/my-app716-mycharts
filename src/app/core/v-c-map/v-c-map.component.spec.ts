import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VCMapComponent } from './v-c-map.component';

describe('VCMapComponent', () => {
  let component: VCMapComponent;
  let fixture: ComponentFixture<VCMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VCMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VCMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
