import { Component, OnInit } from '@angular/core';
import {EChartOption} from 'echarts';
import * as echarts from 'echarts';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-v-c-map',
  templateUrl: './v-c-map.component.html',
  styleUrls: ['./v-c-map.component.less']
})
export class VCMapComponent implements OnInit {
  regionOptions: EChartOption = {}
  data = [{
    name: "海门",
    value: 12
  },
    {
      name: "鄂尔多斯",
      value: 30
    }];

  geoCoordMap = {
    "海门": [121.15, 31.89],
    "鄂尔多斯": [109.781327, 39.608266]
  };
  constructor(private http: HttpClient) { }
  convertData(data) {
    var res = [];
    for (var i = 0; i < data.length; i++) {
      var geoCoord = this.geoCoordMap[data[i].name];
      if (geoCoord) {
        res.push({
          name: data[i].name,
          value: geoCoord.concat(data[i].value)
        });
      }
    }
    return res;
  }
  ngOnInit(): void {
    this.http.get('./assets/json/china.json')
      .subscribe(geoJson => {
        echarts.registerMap('china', geoJson);
        this.regionOptions = {
          geo: {
            show: true,
            map: 'china',
            label: {
              emphasis: {
                show: true,
                color: '#fff'
              }
            },
            roam: false,
            itemStyle: {
              normal: {
                areaColor: '#01215c',
                borderWidth: 1,//设置外层边框
                borderColor:'#9ffcff',
                shadowColor: 'rgba(0,54,255, 1)',
                shadowBlur: 30
              },
              emphasis:{
                areaColor: '#01215c',
              }
            }
          },
          tooltip: {
            trigger: 'item',

          },
          series: [
            {
              type: 'map',
              map: 'china',
              geoIndex: 1,
              aspectScale: 0.75, //长宽比
              showLegendSymbol: false, // 存在legend时显示
              tooltip:{
                show:false
              },
              label: {
                normal: {
                  show: false
                },
                emphasis: {
                  show: false
                }
              } ,
              roam: false,

              itemStyle: {
                normal: {
                  areaColor: '#01215c',
                  borderColor: '#3074d0',
                  borderWidth: 1
                },
                emphasis: {
                  areaColor: '#01215c'
                }
              },
            },{
              name: '散点',
              type: 'effectScatter',
              coordinateSystem: 'geo',
              data: this.convertData(this.data),
              symbolSize: 20,
              symbol: 'circle',
// 			symbolSize: function (val) {
// 				return val[2];
// 			},
              label: {
                normal: {
                  show: false
                },
                emphasis: {
                  show: false
                }
              } ,
              showEffectOn: 'render',
              itemStyle: {
                normal: {
                  color: {
                    type: 'radial',
                    x: 0.5,
                    y: 0.5,
                    r: 0.5,
                    colorStops: [{
                      offset: 0,
                      color: 'rgba(14,245,209,0.2)'
                    }, {
                      offset: 0.8,
                      color: 'rgba(14,245,209,0.2)'
                    }, {
                      offset: 1,
                      color: 'rgba(14,245,209,1)'
                    }],
                    global: false // 缺省为 false
                  },
                }

              },

            },
          ]
        };
      });
    window.addEventListener("resize", function () {
      echarts.init(document.getElementById('map')).resize();
    });
  }




}
